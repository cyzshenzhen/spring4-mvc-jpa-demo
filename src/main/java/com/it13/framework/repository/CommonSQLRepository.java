package com.it13.framework.repository;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import com.it13.framework.domain.BaseDomain;

/**
 * 公用本地SQL Repository
 * 
 * @author yzChen  2015-3-28 10:25:53
 * @param <T>
 */
@Repository
public class CommonSQLRepository<T extends BaseDomain> extends BaseSQLRepository<T> {

	public T queryEntryDataByEntryId(Class<T> targetClass, String tableName, String entryId) throws EmptyResultDataAccessException {
		String sql = "select * from " + tableName + " where id = ?0";
		return this.queryById(targetClass, sql, entryId);
	}
	
}
