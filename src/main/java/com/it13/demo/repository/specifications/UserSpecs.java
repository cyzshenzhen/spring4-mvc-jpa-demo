package com.it13.demo.repository.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.it13.demo.domain.User;

public class UserSpecs {

	public static Specification<User> equalBalanceAndlikeUserName(
			final User user) {

		return new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {

				// add for not null
				Predicate predicate = builder.and();

				// condition one
				if (user.getIsDeleted() != null) {
					predicate = builder.and(builder.equal(
							root.get("isDeleted"), user.getIsDeleted()));
				}
				// condition two
				if (user.getName() != null) {
					String likePattern = getLikePattern(user.getName());
					
					predicate = builder.and(predicate, builder.like(
							builder.lower(root.<User> get("user").<String> get(
									"name")), likePattern));
				}

				return predicate;
			}

			private String getLikePattern(final String searchTerm) {
				StringBuilder pattern = new StringBuilder();
				pattern.append("%");
				pattern.append(searchTerm.toLowerCase());
				pattern.append("%");
				return pattern.toString();
			}
		};
	}
}
