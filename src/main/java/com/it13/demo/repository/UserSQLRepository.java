package com.it13.demo.repository;

import org.springframework.stereotype.Repository;

import com.it13.demo.domain.User;
import com.it13.framework.repository.CommonSQLRepository;

@Repository
public class UserSQLRepository extends CommonSQLRepository<User> {

	public User findOne4NativeSQLQuery(Long id) {
		return super.queryEntryDataByEntryId(User.class, "USER", String.valueOf(id));
	}
	
	
}
